Project Name: IQCalendar

Description: Incomplete implementation of the IQCalendar focusing on displaying of the calendar, month toggle and the ability to add and display tasks
Implementation uses the MVC Pattern utilizing the SAP UI5 framework and makes the assumption that the reader is familure with the lifecycle that the 
framework provides. 

Author: Myles Holt

Contact: mylesnewtonholt@gmail.com

Framework & Libaries: SAP UI5, RequireJS, JQuery

Copyright: Myles Holt 2016

Files & Folders:
    *IQCalendar/
        *html/
        *scripts/ 
            *ui/
                *calendar/
                *model/ 
                *widgets/ 
            *libs/

 

