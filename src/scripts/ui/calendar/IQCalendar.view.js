"use strict";

jQuery.sap.require("sap.ui.unified.Menu");
jQuery.sap.require("sap.ui.core.Popup");
 

sap.ui.jsview("myles.calendar.IQCalendar", {

    getControllerName: function() {
        return "myles.calendar.IQCalendarControl";
    },

    createContent: function(oController) {
        
        this.oIQCalendar.setModel(oController.oIQCalendarModel);

        this.oIQCalendar = new sap.ui.unified.Calendar({
            properties {
                intervalSelection : false,
                singleSelection : true,
                months : 12,
                firstDayOfWeek : 0,
            },
            aggregations {
                selectedDates : {},
                specialDates : {}
            },
            events {
                creatNewTask : {},
                deleteTask : {}
            }
            
        });

        this._initCalendar(oController);
        
        this._attachCalendarListeners();

        return this.oIQCalendar;
    },

    _initCalendar: function(oController) {

        this._CALENDAR_PREVOUS = "Previous";
        this._CALENDAR_NEXT = "Next";

        var oActionsMenu = new sap.ui.unified.Menu({
            placement : sap.m.PlacementType.Top,
            showCancelButton : false,
            buttons : []
        }).addStyleClass('calendarMenuStyle');

        var oPreviousMonthButton = new sap.ui.commons.Button({
            icon : resouceBundle.getIcon("icon_previous"),
            enabled : true,
            press : oController._calendarFetchAndUpdateMonth(this._CALENDAR_PREVOUS)

        }).addStyleClass("monthToggleButtonStyle");  

        oActionsMenu.addItem(oPreviousMonthButton);

        var oNextMonthButton = new sap.ui.commons.Button({
            icon : resouceBundle.getIcon("icon_next"),
            enabled : true,
            press : oController._calendarFetchAndUpdateMonth(this._CALENDAR_NEXT)

        }).addStyleClass("monthToggleButtonStyle");  

        oActionsMenu.addItem(oNextMonthButton);

        /* More UI Controls to follow to properly render the calendar
           ...
        */

    },

    _setCurrentMonth: function(dateValue) {
        this._dateTextField.setValue(dateValue.month + " " + dateValue.year);
    },

    /* Invoked when we change the month to re-render new content */
    _updateCalendar: function() {
        if (this.oIQCalendar) {
            this.oController.save();
            this.oIQCalendar.destroy();
        }

        this.createContent(this.oController);
    },

    _attachCalendarListeners: function() {
        this._attachCreatNewTask(function(oEvent) {
            
            var taskType = oEvent.getTaskType();
           
            var oNewTaskContent = {
                // build content for the new task dialog
                if (taskType === TaskType.SHARED) {
                    // build shared content
                } else {
                    // build private content
                }
            }
            // ofnOnOkCallBack function to update the calendar with the 
            // newly created task
            var oTaskDialog = myles.calendar.IQCalendar.TaskDialog("new-task", {
                title : IQCalendarHelper.getTaskTitle(),
                content : oNewTaskContent,
                beginCallBack : $.proxy(ofnOnOKCallBack, null, oNewTaskContent)
            }).attachAfterClose(null, function() {
                this.destroy();
            });

            oTaskDialog.open();
         });

        }
        
    },


    exit: function() {
        if (this.oIQCalendar) {
            this.oIQCalendar.destroy();
            delete this.oIQCalendar;
        }
    },

    renderer: function(oRm, oControl) {
        oRm.write("<div");
        oRm.writeControlData(oControl);
        oRm.writeClasses();
        oRm.write(">");

        oRm.renderControl(oControl.getControl());

        oRm.write("</div>");
    },

});