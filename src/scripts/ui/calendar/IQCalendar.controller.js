"use strict";

jQuery.sap.require("sap.ui.model.json.JSONModel");

sap.ui.controller("myles.calendar.IQCalendar", {
    oIQCalendarModel: new sap.ui.model.json.JSONModel(),
    oPersistenceManager: new myles.calendar.IQCalendar.PersistenceManager(),
    this._monthArray = ["January", "Feburary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    onInit: function() {
    },

    onExit: function() {
    },

    onBeforeRendering: function() {
        this.fetchData();
    },

    fetchData: function(dateValue) {
        this.getView().setBusy(true);
        // if dateValue is undefined use current system time
        if (dateValue === undefined){
            var today = new Date();
            dateValue.year = today.getYear();
            dateValue.month = today.getMonth();
        }

        var oPromise = this.oPersistenceManager.fetchData(dateValue);
        oPromise.done(function(oCalendarData) {
            this.oIQCalendarModel.setData(oCalendarData);
            this.getView().setBusy(false);
        }.bind(this));

        oPromise.fail(function(oException) {
            this.oView.setErrorState();
            this.getView().setBusy(false);
        }.bind(this));
    },

    _calendarFetchAndUpdateMonth: function(direction) {
        // currentMonthVaue and currentYear stored as integers
        var currentMonthValue = this.oIQCalendarModel.getProperty("/currentMonthValue");
        var currentYear = this.oIQCalendarModel.getProperty("/currentYear");
        var newYear;
        var newMonth;
        if (direction === oView._CALENDAR_PREVOUS) {
            if (currentMonthValue === 0) {
                newMonth = this._monthArray[11];
                newYear = currentYear-1;
            } else {
                newMonth = this._monthArray[currentMonthValue-1];
            }
            this.fetchData({year: newYear, month: newMonth})
        } else if (direction === oView._CALENDAR_NEXT) {
            if (currentMonthValue === 11) {
                newMonth = this._monthArray[0];
                newYear = currentYear+1;
            } else {
                newMonth = this._monthArray[currentMonthValue+1];
            }
            this.fetchData({year: newYear, month: newMonth})
        }

        this.oView._updateCalendar();
    },

    _save: function() {
        this.getView().setBusy(true);
        
        var oPromise = this.oPersistenceManager.saveData();
        oPromise.done(function(oCalendarData) {
            this.getView().setBusy(false);
        }.bind(this));

        oPromise.fail(function(oException) {
            this.oView.setErrorState();
            this.getView().setBusy(false);
        }.bind(this));
    }

});